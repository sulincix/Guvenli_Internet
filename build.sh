allowed(){
while read line
do
echo "$line" | grep -v "^instagram.com$" | grep -fv "^instagram.com$" "^m.instagram.com$" "^facebook.com$" "^m.facebook.com$"
done

}

rm -rf temp
mkdir temp
curl "https://raw.githubusercontent.com/adversarialtools/apple-telemetry/master/blacklist" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/apple-telemetry
curl "https://raw.githubusercontent.com/EnergizedProtection/EnergizedHosts/master/EnergizedUltimate/energized/hosts" > temp/energized
curl "https://raw.githubusercontent.com/arthurgeron/blockYTAds/master/hosts.txt" > temp/ytadblock
#curl "https://raw.githubusercontent.com/anudeepND/youtubeadsblacklist/master/hosts.txt" > temp/ytvid # Youtube video content 
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt" > temp/wspy 
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra.txt" > temp/wspyextra
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update.txt" > temp/wspyupdate
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy_v6.txt" > temp/wspyv6
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra_v6.txt" > temp/wspyextrav6
curl "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update_v6.txt" > temp/wspyupdatev6
curl "https://v.firebog.net/hosts/static/w3kbl.txt" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/w3kbl
curl "https://raw.githubusercontent.com/vokins/yhosts/master/hosts.txt" > temp/yhosts
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts" > temp/unchecky
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/StreamingAds/hosts" > temp/streamads
curl "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling/hosts" > temp/fakenews
curl "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext" > temp/yoyo
curl "https://someonewhocares.org/hosts/zero/hosts" > temp/zero
curl "https://raw.githubusercontent.com/CHEF-KOCH/NSABlocklist/master/HOSTS/HOSTS" > temp/chef-koch
curl "https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt" > temp/notracking
curl "https://hosts.nfz.moe/full/hosts" > temp/moe
curl "http://winhelp2002.mvps.org/hosts.txt" > temp/mvps
curl "http://malwaredomains.lehigh.edu/files/immortal_domains.txt" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/malware-important
curl "http://www.malwaredomainlist.com/hostslist/hosts.txt" > temp/malwaredomains
curl "https://raw.githubusercontent.com/azet12/KADhosts/master/KADhosts.txt" > temp/kadhosts
curl "http://rlwpx.free.fr/WPFF/hrsk.7z" > temp/hrsk.7z ; 7z x temp/hrsk.7z ; mv Hosts.rsk temp/Hosts.rsk; rm -f temp/hrsk.7z 
curl "https://hosts-file.net/wrz.txt" > temp/wrz
curl "https://hosts-file.net/mmt.txt" > temp/mmt
curl "https://hosts-file.net/hjk.txt" > temp/hjk
curl "https://hosts-file.net/grm.txt" > temp/grm
curl "https://hosts-file.net/exp.txt" > temp/exp
curl "https://hosts-file.net/emd.txt" > temp/emd
curl "https://hosts-file.net/ad_servers.txt" > temp/ad_servers
curl "https://hostsfile.mine.nu/hosts0.txt" > temp/host0
curl "https://www.dshield.org/feeds/suspiciousdomains_Low.txt" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/dshield
curl "https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/simple_malvertising
curl "https://zerodot1.gitlab.io/CoinBlockerLists/hosts" > temp/coinblocker
curl "https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser" > temp/coinblocker_browser
curl "http://sysctl.org/cameleon/hosts.win" > temp/camelon
curl "https://raw.githubusercontent.com/mitchellkrogza/The-Big-List-of-Hacked-Malware-Web-Sites/master/hacked-domains.list" | grep -v "^#" | sed "s/^/0.0.0.0 /g" > temp/biglist
curl "https://ssl.bblck.me/blacklists/hosts-file.txt" > temp/barbblock
curl "https://raw.githubusercontent.com/mitchellkrogza/Badd-Boyz-Hosts/master/hosts" > temp/badd
curl "https://raw.githubusercontent.com/Yhonay/antipopads/master/hosts" > temp/anti-popads
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts" > temp/add.spam
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts" > temp/add.risk
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Dead/hosts" > temp/add.dead
curl "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts" > temp/add.2to7
curl "https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt" > temp/adaway
curl "https://logroid.github.io/blogger/file/hosts.txt" > temp/logroid
#curl "https://raw.githubusercontent.com/eladkarako/hosts/master/build/hosts.txt" > temp/eladkarako # may be broke website
curl "https://raw.githubusercontent.com/yous/YousList/master/hosts.txt" > temp/yous
curl "https://raw.githubusercontent.com/lewisje/jansal/master/adblock/hosts" > temp/jansal
cat temp/* >> hosts_new
cat hosts_new | grep -v "^#" | grep -v "^!" | grep -v "^ "| grep -v "^\t" | grep -v "^white" | sort | uniq > hosts
rm -f hosts_new
